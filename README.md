# **Workflows with Docker** #



## 1. React  ##

* Launch init.sh and you're done !
* localhost:3000 for app
* back:4000 for api requests (proxied, access by localhost:3000/api/your-query)
* db:3306 from internal or localhost:3306 on outside for db
* Production builds soon

## 2. Php learning ##

* init.sh for the rescue !
* localhost:3000 for app
* database same as in react
